const express = require('express'); //referencia al paquete express
const bodyParser = require('body-parser');
const userFile = require('./user.json');
var app = express(); // creamos el servidor de Node
app.use(bodyParser.json()); // usamos el body-parser
var puerto = process.env.PORT || 3000;
const URL_BASE = '/api-peru/v1/';
const requestJSON=require('request-json');
//const mLabURLbase = 'https://api.mlab.com/api/1/databases/techu15db/collections/';
//const apikeyMLab = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';

app.get(URL_BASE + 'users',
  function(req, res) {
    const httpClient = requestJSON.createClient(mLabURLbase);
    console.log("Cliente HTTP mLab creado.");
    const fieldParam = 'f={"_id":0}&';
    httpClient.get('user?' + fieldParam+ apikeyMLab,
      function(err, respuestaMLab, body) {
        console.log('Error: ' + err);
        console.log('Respuesta MLab: ' + respuestaMLab);
        console.log('Body: ' + body);
        var response = {};
        if(err) {
            response = {"msg" : "Error al recuperar users de mLab."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."};
            res.status(404);
          }
        }
        res.send(response);
      }); //httpClient.get(
});

// Petición GET id con mLab
app.get(URL_BASE + 'users/:id',
  function (req, res) {
    console.log("GET /colapi/v3/users/:id");
    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(mLabURLbase);
    httpClient.get('user?' + queryString + queryStrField + apikeyMLab,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");
      //  var respuesta = body[0];
        var response = {};
        if(err) {
            response = {"msg" : "Error obteniendo usuario."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."}
            res.status(404);
          }
        }
        res.send(response);
      });
});

// GEt a todos los usuarios: user.json
app.get (URL_BASE + 'users2',
function(request, response) {
//  response.send('Hola Universo');
  response.status(200);
  response.send(userFile);
});

// Peticion GET con Query String
app.get (URL_BASE + 'usersq',
function(request, response) {
  var ind=1;
  var arr = [];
  let objQuery = request.query.max;
  console.log(objQuery);
while (ind <= objQuery) {
  arr.push(userFile[ind-1]);
  ind=ind+1;
}
  response.status(200);
  response.send(arr);
});

// operacion POST a users
app.post (URL_BASE + 'users',
function(req, res) {
  console.log(req.body);
  let lon = userFile.length;
  console.log(lon);
  let nuevoUsu = {
    id: ++lon,
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
    password: req.body.password,
  }
  userFile.push(nuevoUsu);
  res.status(201).send({"mensaje":"usuario creado correctamente","Actualizado": userFile});
});

app.delete (URL_BASE + 'users/:id',
function(req, res) {
  let indice = req.params.id;
  let estado = (userFile[indice-1]== undefined) ? 0:1;
  if(estado==0) {
    let respuesta = {"msg": "No existe"};
    console.log(respuesta);
    res.status(201).send(respuesta);
    }
  else{
    let respuesta = {"Cliente Eliminado": userFile.splice(indice-1,1)};
    console.log(respuesta);
    res.status(201).send({"mensaje":"Cliente Eliminado correctamente","Actualizado": userFile});
   }
});

// PUT a users. Luis Ormeño Cáceres. AQUI ESTA LA SEGUNDA TAREA %%%%%%%%%%%%%%
// PUT a users. Luis Ormeño Cáceres. AQUI ESTA LA SEGUNDA TAREA %%%%%%%%%%%%%%
// Por ahora se actualiza también el "Id", sin embargo, podría dejar de actualizarse.
app.put (URL_BASE + 'users/:id',
function(req, res) {
  let indice = req.params.id;
  console.log(req.body);
  console.log(indice);
  let userFileUpd = (userFile[indice-1]== undefined) ?
                     {"msg": "Cliente No existe"}:userFile[indice-1];
  console.log(userFileUpd);
  let updUsu = {
    id: req.body.id,
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
    password: req.body.password,
  };
  userFileUpd = updUsu;
  userFile[indice-1] = userFileUpd;
  res.status(201).send({"mensaje":"usuario Actualizado correctamente","Actualizado": userFile});
});

app.get (URL_BASE + 'users/:id',
function(request, response) {
  let indice = request.params.id;
  let respuesta = (userFile[indice-1]== undefined) ? {"msg": "No existe"}:userFile[indice-1];
  console.log(respuesta);
  response.status(200).send(respuesta);
});

app.get (URL_BASE + 'userstotal',
function(request, response) {
  let lon = userFile.length;
  let lonJson = JSON.stringify({num_elem: lon});
//  console.log(lon);
  response.status(200).send(lonJson);
});

// LOGIN - user.json
app.post(URL_BASE + 'login',
  function(request, response) {
  // console.log("POST /apitechu/v1/login");
   console.log(request.body.email);
   console.log(request.body.password);
   var user = request.body.email;
   var pass = request.body.password;
   var fin = false;
   for(us of userFile) {
     if(us.email == user) {
       if(us.password == pass) {
         if(us.logged) {
           console.log("Usuario ya logueado");
           response.send({"msg" : "El usuario ya se encuentra logueado"});
         }
          else {
              us.logged = true;
              fin = true;
              writeUserDataToFile(userFile);
              console.log("Login correcto!");
              response.send({"msg" : "Usuario logueado correctamente.",
                             "idUsuario" : us.id,
                             "logged" : "true"});
          }
       }
        else {
             console.log("Login incorrecto.");
             response.send({"msg" : "Login incorrecto. Password inválido"});
        }
     }
    }
    if(!fin)  {
      console.log("Login incorrecto.");
      response.send({"msg" : "Login incorrecto. Email no existe"});
    }
});

// LOGOUT - users.json
 app.post(URL_BASE + 'logout',
  function(request, response) {
   //console.log("POST /apitechu/v1/logout");
   var userId = request.body.id;
   for(us of userFile) {
    if(us.id == userId) {
     if(us.logged) {
       delete us.logged; // borramos propiedad 'logged'
       writeUserDataToFile(userFile);
       console.log("Logout correcto!");
       response.send({"msg" : "Logout correcto.", "idUsuario" : us.id});
      } else {
       console.log("Logout incorrecto.");
       response.send({"msg" : "Logout incorrecto."});
      }
     }
    }
  });

function writeUserDataToFile(data) {
    var fs = require('fs');
    var jsonUserData = JSON.stringify(data);
    fs.writeFile("./user.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
       console.log(err);
      } else {
       console.log("Datos escritos en 'user.json'.");
      }
    })
}

// http://localhost:3000/fred
app.listen(puerto,function(){
  console.log('Node JS escuchando puerto: 3000');
});
